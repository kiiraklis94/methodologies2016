package lab5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;
import javax.swing.JOptionPane;

/**
 *
 * @author amenychtas
 */
public class ProcessFiles {

    public static void main(String[] args) {
        processUpsideDown("README.md", "UpsideDown.txt");

        int lineLength = getLineLength();
        processLineLength("README.md", "OnlyShortLines.txt", lineLength);
    }

    /**
     * The method reads a positive integer number from an InputDialog.
     *
     * @return the number provided by the user.
     */
    private static int getLineLength() {
        do {
            //Read the number of integers
            String s = JOptionPane.showInputDialog("What is the maximum length for each line");

            //Check if s is a valid number
            try {
                int n = Integer.parseInt(s);

                //Check if the number is positive 
                if (n > 0) {
                    //if we have a valid integer the methods returns and the while finishes
                    return n;
                } else {
                    JOptionPane.showMessageDialog(null, "Please provide a number > 0.");
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "\"" + s + "\" is not a valid number.");
            }
        } while (true);
    }

    /**
     * Copies the input file into the output file filtering the lines that are
     * longer than a length provided.
     *
     * @param inputFileName The name of the input file.
     * @param outputFileName The name of the output file.
     * @param lineLength The maximum length of the line in the output file.
     */
    private static void processLineLength(String inputFileName, String outputFileName, int lineLength) {
        BufferedReader input = null;
        BufferedWriter output = null;

        try {
            input = new BufferedReader(new FileReader(inputFileName));
            output = new BufferedWriter(new FileWriter(outputFileName));

            String line;
            while ((line = input.readLine()) != null) {
                if (line.length() < lineLength) {
                    output.write(line);
                    output.newLine();
                }
            }

        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                    System.err.println(ex.getMessage());
                }
            }
            if (output != null) {
                try {
                    output.close();
                } catch (IOException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }

    /**
     * Copies the input file into the output file with the lines reversed (the
     * first line in the input file is the last in the output etc.) using a
     * stack.
     *
     * @param inputFileName The name of the input file.
     * @param outputFileName The name of the output file.
     */
    private static void processUpsideDown(String inputFileName, String outputFileName) {
        BufferedReader input = null;
        BufferedWriter output = null;

        try {
            input = new BufferedReader(new FileReader(inputFileName));
            output = new BufferedWriter(new FileWriter(outputFileName));

            String line;

            //We use Deque instead of Stack which is considered legacy code.
            //Stack<String> stack = new Stack<>();
            Deque<String> stack = new ArrayDeque<>();

            //puts each line of the input file into the stack
            while ((line = input.readLine()) != null) {
                stack.push(line);
            }

            //copies each line of the stack into the output file
            while (!stack.isEmpty()) {
                output.write(stack.pop());
                output.newLine();
            }

        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                    System.err.println(ex.getMessage());
                }
            }
            if (output != null) {
                try {
                    output.close();
                } catch (IOException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }
}
