package lab5.windows;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * This class implements the ActionListner interface and
 * will be used for the "listening" the action events
 * of the button in QuickAndDirtyWindow.
 */
public class MyListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Test Message");
    }
    
}