package lab5;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;

public class FilesPlay {

    public static void main(String[] args) {
        //powerOf2();
        //aBetterPowerOf2();

        //readBytes("README.md");
        //writeChars("test.txt", "Hello");
        //aBetterWriteChars("test.txt", "Hello");
    }

    private static void powerOf2() {
        String s = JOptionPane.showInputDialog("Give us a number:");
        int number = Integer.parseInt(s);
        JOptionPane.showMessageDialog(null, "" + number * number);
    }

    private static void aBetterPowerOf2() {
        String s = JOptionPane.showInputDialog("Give us a number:");
        try {
            int number = Integer.parseInt(s);
            JOptionPane.showMessageDialog(null, String.valueOf(number * number));

            //Unchecked Exception
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null, s + " is not a number!");
        }
    }

    //Reading bytes from a file using streams...
    private static void readBytes(String fileName) {
        try {
            FileInputStream fis = new FileInputStream(fileName);
            boolean eof = false;
            while (!eof) {
                int i = fis.read();
                if (i >= 0) {
                    System.out.print((char) i);
                } else {
                    eof = true;
                }
            }
            fis.close();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    //Reading characters 
    private static void readChars(String fileName) {
        try {
            FileReader fr = new FileReader(fileName);
            boolean eof = false;
            while (!eof) {
                int i = fr.read();
                if (i >= 0) {
                    System.out.print((char) i);
                } else {
                    eof = true;
                }
            }
            fr.close();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private static void aBetterReadBytes(String fileName) {
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(fileName));

            int i;
            while ((i = bis.read()) > -1) {
                System.out.print((char) i);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            //We need to close the stream if it has been initialized i.e. bis != null
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }

    private static void aBetterReadChars(String fileName) {
        //Here we use "try with resources" instead of the "finally block"
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileName))) {

            int i;
            while ((i = bis.read()) > -1) {
                System.out.print((char) i);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private static void writeChars(String fileName, String text) {
        FileWriter fw = null;
        try {
            fw = new FileWriter(fileName);
            //or the following to append the new content at the end of the file
            //fw = new FileWriter(fileName, true); 
            fw.write(text);
            fw.close();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private static void aBetterWriteChars(String fileName, String text) {
        try (PrintWriter pw = new PrintWriter(fileName)) {
            pw.println(text);
        } catch (FileNotFoundException ex) {
            System.err.println(ex.getMessage());
        }
    }
}