package lab6;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author amenychtas
 */
public class AnotherWindow extends JFrame implements ActionListener {

    //We put the window creation code in the constructor
    public AnotherWindow() {
        setTitle("Another Window.");
        setSize(500, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new FlowLayout());

        JButton button = new JButton();
        button.setText("Press me");

        //Instead of using the MyListener class we use this!
        //Since this (AnotherWindow) implements the ActionListener too.
        button.addActionListener(this);
        
        add(button);                
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Another Test Message");
    }

    public static void main(String[] args) {
        //We could do the following but this means that the UI thread
        //of JFrame thread will be the same with this thread.
        //AnotherWindow aw = new AnotherWindow();
        //aw.setVisible(true);

        //With the following code though we start a new thread to run
        //the window.
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                AnotherWindow aw = new AnotherWindow();
                aw.setVisible(true);
            }
        });
    }
}

