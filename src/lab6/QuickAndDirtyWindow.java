package lab6;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * Example of window in Java using JFrame. This is improved in the AnotherWindow
 * class to highlight the mistakes and assumptions that we have here.
 */
public class QuickAndDirtyWindow {

    public static void main(String[] args) {
        JFrame myWindow = new JFrame();
        myWindow.setTitle("My first window!");
        myWindow.setSize(500, 500);
        myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Places the window at the center of the screen.
        myWindow.setLocationRelativeTo(null);

        //Changes the layout from the default (BorderLayout) to the 
        //simple FlowLayout
        myWindow.setLayout(new FlowLayout());

        //A new button is created.
        JButton button = new JButton();
        button.setText("Press me");

        //Adds the a new instance of the class MyListerer to 
        //the button as ActionLister
        button.addActionListener(new MyListener());

        //Adds the button into JFrame
        myWindow.add(button);
        
        //Makes the window visible
        myWindow.setVisible(true);
    }
}