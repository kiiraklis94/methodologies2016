/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author amenychtas
 */
public class CalculatorWindow extends JFrame implements ActionListener {

    private JTextField textA;
    private JTextField textB;
    private JTextField outputText;

    public CalculatorWindow() {
        setTitle("Another Window.");
        setSize(500, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JPanel inputPanel = new JPanel();
        inputPanel.add(new JLabel("Number A:"));
        textA = new JTextField(5);
        inputPanel.add(textA);
        inputPanel.add(new JLabel("Number B:"));
        textB = new JTextField(5);
        inputPanel.add(textB);

        inputPanel.setBorder(BorderFactory.createTitledBorder("Input"));

        add(inputPanel, BorderLayout.NORTH);

        JPanel outputPanel = new JPanel();
        JButton button = new JButton("Calculate");
        button.addActionListener(this);
        outputPanel.add(button);
        outputText = new JTextField(10);
        outputText.setEditable(false);
        outputPanel.add(outputText);

        outputPanel.setBorder(BorderFactory.createTitledBorder("Output"));

        add(outputPanel, BorderLayout.SOUTH);

        pack();
    }

    public static void main(String[] args) {
        CalculatorWindow cw = new CalculatorWindow();
        cw.setVisible(true);

        //new CalculatorWindow().setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            String a = textA.getText();
            String b = textB.getText();

            int intA = Integer.parseInt(a);
            int intB = Integer.parseInt(b);

            outputText.setText(String.valueOf(intA + intB));
        } catch (NumberFormatException ex) {
            outputText.setText("Invalid input!");
        }
    }

}
