package lab2;

public class Car {
    //Should the members below be public or private?
    public String make;
    //Assume that the plates is a four digit number
    private int carPlates;
    //We could use the Class java.awt.Color. 
    //Try it home, and remember: "JavaDoc is a good friend".   
    public String color;

    //Consider adding methods to set and get the above variables 
    //So as to ensure valid values
    public void setCarPlates(int a) {
        if (a > 999 && a < 10000) {
            carPlates = a;
        } else {
            System.out.println("Error");
        }
    }

    public void print() {
        System.out.println("This " + color + " " + make
                + " has " + carPlates + " license number");
    }
}
