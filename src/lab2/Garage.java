package lab2;

public class Garage {

    private Car[] parkedCars = new Car[10];

    //Implement the method
    public int getParkedCarsNumber() {

        return 0;
    }

    public boolean isFull() {
        for (Car parkedCar : parkedCars) {
            if (parkedCar == null) {
                return false;
            }
        }
        return true;
    }

    //Add the methods to park and unpark cars. In the methods body add print messages 
    //to see what's really happening. 
    public boolean park(Car car) {
        for (int i = 0; i < parkedCars.length; i++) {
            if (parkedCars[i] == null) {
                parkedCars[i] = car;
                return true;
            }
        }
        return false;
    }
}
