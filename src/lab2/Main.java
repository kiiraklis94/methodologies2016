package lab2;

public class Main {
        public static void main(String[] args) {
        Car car1 = new Car();
        car1.make = "ford";
        car1.setCarPlates(4389);
        car1.color = "red";
        car1.print();

        Car car2 = new Car();
        //check what is printed
        car2.print();

        Garage g = new Garage();
        
        boolean check = g.park(car1);
        System.out.println(check);

        System.out.println(g.park(car2));
    }
}
