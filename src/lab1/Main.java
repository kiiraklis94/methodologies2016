package lab1;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello");

        Car myCar = new Car();
        myCar.color = "red";
        myCar.make = "ford";
        myCar.seats = 5;

        System.out.println("The car color is " + myCar.color + "\n"
                + "the make is " + myCar.make + "\n"
                + "and it has " + myCar.seats + " seats.");

        Car yourCar = new Car();
        System.out.println("The car color is " + yourCar.color + "\n"
                + "the make is " + yourCar.make + "\n"
                + "and it has " + yourCar.seats + " seats.");

        Car anotherCar = myCar;
        System.out.println("The car color is " + anotherCar.color + "\n"
                + "the make is " + anotherCar.make + "\n"
                + "and it has " + anotherCar.seats + " seats.");
    }
}
