package lab7_8;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.Timer;

public class ClockLabel extends JLabel implements ActionListener{
    private Timer t;
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");      
    
    public ClockLabel() {
        t = new Timer(1000, this);
        t.start();
    }    
    
    private void updateClock() {
        Date d = new Date();        
        setText(sdf.format(d));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        updateClock();
    }
    
    public void startClock(){
        t.start();
    }
    
    public void stopClock(){
        t.stop();
    }
    
    public void updateFormat(String s) {
        sdf = new SimpleDateFormat(s);
        updateClock();
    }
}
