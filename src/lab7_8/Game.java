package lab7_8;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Game extends JFrame implements ActionListener {

    private JButton rockButton;
    private JButton scissorsButton;
    private JButton paperButton;
    private JLabel resultLabel;
    private JLabel computerSelectionLabel;

    public Game() {
        setTitle("Game");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JPanel userPanel = new JPanel();
        userPanel.setBorder(BorderFactory.createTitledBorder("Make your choice!"));

        ImageIcon rockIcon = new ImageIcon("rock.png");
        rockButton = new JButton(rockIcon);
        rockButton.addActionListener(this);
        userPanel.add(rockButton);

        ImageIcon scissorsIcon = new ImageIcon("scissors.png");
        scissorsButton = new JButton(scissorsIcon);
        scissorsButton.addActionListener(this);
        userPanel.add(scissorsButton);

        ImageIcon paperIcon = new ImageIcon("paper.png");
        paperButton = new JButton(paperIcon);
        paperButton.addActionListener(this);
        userPanel.add(paperButton);

        add(userPanel, BorderLayout.NORTH);

        JPanel resultPanel = new JPanel();
        resultPanel.setBorder(BorderFactory.createTitledBorder("Result"));

        computerSelectionLabel = new JLabel(" ");
        resultPanel.add(computerSelectionLabel);

        resultLabel = new JLabel(" ");
        resultPanel.add(resultLabel);

        add(resultPanel, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(null);
    }

    public static void main(String[] args) {
        new Game().setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int userSelection = 0;
        if (e.getSource() == rockButton) {
            userSelection = 0;
        } else if (e.getSource() == scissorsButton) {
            userSelection = 1;
        } else if (e.getSource() == paperButton) {
            userSelection = 2;
        }

        Random r = new Random();
        int computerSelection = r.nextInt(3);

        switch (computerSelection) {
            case 0:
                computerSelectionLabel.setText("Computer selected: Rock");
                break;
            case 1:
                computerSelectionLabel.setText("Computer selected: Scissors");
                break;
            case 2:
                computerSelectionLabel.setText("Computer selected: Paper");
                break;
        }

        if (userSelection == computerSelection) {
            resultLabel.setText("Draw!");
        } else if ((userSelection == 0 && computerSelection == 1)
                || (userSelection == 1 && computerSelection == 2)
                || (userSelection == 2 && computerSelection == 0)) {
            resultLabel.setText("You Won!");
        } else {
            resultLabel.setText("You Lost!");
        }
    }
}
