package lab7_8;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

public class MyWindow extends JFrame implements ActionListener {

    private JButton startButton;
    private JButton stopButton;
    private ClockLabel cl;
    private JComboBox<String> dateList;

    public MyWindow() {
        setTitle("Clock");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(500, 500);
        setLocationRelativeTo(null);

        setLayout(new FlowLayout());

        cl = new ClockLabel();
        add(cl);

        startButton = new JButton("Start");
        startButton.addActionListener(this);
        add(startButton);
        stopButton = new JButton("Stop");
        stopButton.addActionListener(this);
        add(stopButton);

        String[] dates = {"HH:mm:ss", "hh:mm:ss a", "HH:mm:ss MMMM dd yyyy"};
        dateList = new JComboBox<>(dates);
        dateList.addActionListener(this);
        add(dateList);
    }

    public static void main(String[] args) {
        new MyWindow().setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(startButton)) {
            cl.startClock();
        } else if (e.getSource().equals(stopButton)) {
            cl.stopClock();
        } else if (e.getSource().equals(dateList)) {
            cl.updateFormat((String) dateList.getSelectedItem());
        }
    }
}
