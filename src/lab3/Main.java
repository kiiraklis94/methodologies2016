package lab3;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Main {

    public static void main(String[] args) {
        System.out.println("I have constructed "
                + Vehicle.countVehicles() + " vehicles");

        Vehicle vehicle = new Vehicle(5);
        System.out.println(vehicle);

        System.out.println("I have constructed "
                + Vehicle.countVehicles() + " vehicles.");

        //We use the static method showInputDialog for interacting with the user
        String captain = JOptionPane.showInputDialog(
                "Please select captain's name");
        Aircraft aircraft = new Aircraft(captain, 180);
        System.out.println(aircraft);
        aircraft.fly();

        System.out.print("How many times to fly? ");

        //Reads an integer from the console
        Scanner scanner = new Scanner(System.in);
        int flights = scanner.nextInt();

        //we call overloaded methods
        aircraft.fly(flights);

        Vessel vessel = new Vessel("Petrol", 400);
        System.out.println(vessel);

        System.out.print("Please select the number of passengers: ");
        int passengers = scanner.nextInt();

        SailBoat sailBoat = new SailBoat(passengers);
        System.out.println(sailBoat);

        System.out.println("I have constructed "
                + Vehicle.countVehicles() + " vehicles.");
    }
}
