package lab3;

public class Aircraft extends Vehicle {

    private final String captain;

    public Aircraft(String captain, int passengers) {
        super(passengers);
        this.captain = captain;
    }

    //Constructor overload
    public Aircraft(int passengers) {
        super(passengers);
        captain = "No Captain";
    }

    //Overload the method fly
    public void fly() {
        System.out.println("I'm flying with " + getPassengers() + " people");
    }

    //Overload the method fly
    public void fly(int i) {
        for (int j = 0; j < i; j++) {
            System.out.println("I'm flying with " + getPassengers() + " people");
        }
    }
}
