package lab3;

public class Vessel extends Vehicle {

    private final String fuelType;

    public Vessel(String fuelType, int passengers) {
        super(passengers);
        this.fuelType = fuelType;
    }

    public String getFuelType() {
        return fuelType;
    }

    @Override
    public String toString() {
        return "This vessel has " + getPassengers()
                + " passengers and uses " + fuelType + " for fuel";
    }
}
