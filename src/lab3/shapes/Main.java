/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3.shapes;

/**
 *
 * @author amenychtas
 */
public class Main {
    public static void main(String[] args) {
        Shape s = new Circle(4);
        Shape s1 = new Square(4);
        
        System.out.println(s.perimeter());
        System.out.println(s1.perimeter());
                
        if (s instanceof Circle) {        
             Circle c = (Circle) s;
        }  
    }
}
