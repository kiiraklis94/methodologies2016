/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3.shapes;

/**
 *
 * @author amenychtas
 */
public class Square extends Shape{
    private final int x;

    public Square(int x) {
        this.x = x;
    }

    @Override
    public float perimeter() {
        return 4 * x;
    }
    
    
}
