/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3.shapes;

/**
 *
 * @author amenychtas
 */
public class Circle extends Shape {
    private final float r;

    public Circle(float r) {
        this.r = r;
    }

    @Override
    public float perimeter() {
         return 2 * (float) Math.PI * r;
    }
    
}
