package lab3;

public class Vehicle {
    private final int passengers;
    private int distance  = 0;

    //Counts how many Vehicles are constructed
    private static int counter = 0;

    public Vehicle(int passengers) {
        this.passengers = passengers;

        //Increases the counter
        counter++;
    }

    //Returns the number of constructed vehicles
    public static int countVehicles() {
        return counter;
    }

    public int getPassengers() {
        return passengers;
    }

    public int getDistance() {
        return distance;
    }

    public void trip(int tripDistance){
        distance += tripDistance;
    }

    @Override
    public String toString() {
        return "I'm a vehicle with " + passengers + " passengers";
    }
}
