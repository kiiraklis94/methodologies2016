package lab4.exercise;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class Main {

    private static List<Integer> numbers = new ArrayList<>();

    public static void main(String[] args) {
        input();
        int i = process();
        output(i);
    }

    public static void input() {
        String s = JOptionPane.showInputDialog(
                "Πληκτρολογήστε ακεραίους χωρισμένους με κενό");
        String[] array = s.split(" ");

        for (String number : array) {
            int i = Integer.parseInt(number);
            numbers.add(i);
        }
    }

    public static int process() {
        int sum = 0;

        for (Integer number : numbers) {
            sum += number;
        }

        return sum / numbers.size();
    }

    public static void output(int i) {
        JOptionPane.showMessageDialog(null, "O MO einai " + i);
    }
}
