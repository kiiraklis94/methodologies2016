package lab11;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.swing.*;

/**
 *
 * @author amenychtas
 */
public class ContactsManager extends JFrame implements ActionListener, KeyListener {

    private final JTextField fileNameField;
    private final JButton loadButton;
    private final JButton saveButton;
    private final JTextField firstNameField;
    private final JTextField lastNameField;
    private final JTextField phoneField;
    private final JButton previousButton;
    private final JButton nextButton;
    private final JButton newContactButton;
    private final JButton saveContactButton;

    //We initialize the contacts list to avoid nullPointerExceptions in the actionPerformed method
    private List<Contact> contacts = new LinkedList<>();
    private int index = 0;

    public ContactsManager() {
        setTitle("Contacts sManager");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JPanel fileLoadPanel = new JPanel();
        fileLoadPanel.setBorder(BorderFactory.createTitledBorder("Select File"));

        fileNameField = new JTextField(25);
        fileNameField.addKeyListener(this);
        fileNameField.setToolTipText("src/lab11/contacts.txt");
        fileLoadPanel.add(fileNameField);

        loadButton = new JButton("Load File");
        loadButton.setEnabled(false);
        loadButton.addActionListener(this);
        fileLoadPanel.add(loadButton);

        saveButton = new JButton("Save File");
        saveButton.addActionListener(this);
        saveButton.setEnabled(false);
        fileLoadPanel.add(saveButton);

        add(fileLoadPanel, BorderLayout.NORTH);

        JPanel contactsPanel = new JPanel();
        contactsPanel.setLayout(new GridLayout(5, 2));
        contactsPanel.setBorder(BorderFactory.createTitledBorder("Edit Contact"));

        contactsPanel.add(new JLabel("First Name:"));
        firstNameField = new JTextField(20);
        contactsPanel.add(firstNameField);

        contactsPanel.add(new JLabel("Last Name:"));
        lastNameField = new JTextField(20);
        contactsPanel.add(lastNameField);

        contactsPanel.add(new JLabel("Phone:"));
        phoneField = new JTextField(20);
        contactsPanel.add(phoneField);

        previousButton = new JButton("<");
        previousButton.addActionListener(this);
        contactsPanel.add(previousButton);

        nextButton = new JButton(">");
        nextButton.addActionListener(this);
        contactsPanel.add(nextButton);

        newContactButton = new JButton("New Contact");
        newContactButton.addActionListener(this);
        contactsPanel.add(newContactButton);

        saveContactButton = new JButton("Save Contact");
        saveContactButton.addActionListener(this);
        contactsPanel.add(saveContactButton);

        add(contactsPanel, BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        String fileName = fileNameField.getText();
        if (fileName.length() > 4 && fileName.endsWith(".txt")) {
            loadButton.setEnabled(true);
            saveButton.setEnabled(true);
        } else {
            loadButton.setEnabled(false);
            saveButton.setEnabled(false);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String fileName = fileNameField.getText();
        switch (e.getActionCommand()) {
            case "Load File":
                contacts = loadFile(fileName);

                JOptionPane.showMessageDialog(null, contacts.size() + " contacts loaded");

                if (!contacts.isEmpty()) {
                    index = 0;
                    showContact();
                }
                break;
            case "Save File":
                if (saveFile(fileName, contacts)) {
                    JOptionPane.showMessageDialog(null, "File Saved");
                }
                break;
            case "<":
                if (index > 0) {
                    index--;
                    showContact();
                }
                break;
            case ">":
                if (index < contacts.size() - 1) {
                    index++;
                } else {
                    index = 0;
                }
                showContact();
                break;
            case "New Contact":
                firstNameField.setText(null);
                lastNameField.setText(null);
                phoneField.setText(null);

                //The index of the new contact
                index = contacts.size();
                break;
            case "Save Contact":
                if (index < contacts.size()) {
                    Contact c = contacts.get(index);
                    c.setFirstName(firstNameField.getText());
                    c.setLastName(lastNameField.getText());
                    c.setPhoneNumber(phoneField.getText());
                } else {
                    Contact c = new Contact(firstNameField.getText(),
                            lastNameField.getText(), phoneField.getText());
                    contacts.add(c);
                }
                break;
        }
    }

    private void showContact() {
        Contact contact = contacts.get(index);
        firstNameField.setText(contact.getFirstName());
        lastNameField.setText(contact.getLastName());
        phoneField.setText(contact.getPhoneNumber());
    }

    private List<Contact> loadFile(String fileName) {
        List<Contact> list = new LinkedList<>();
        //or List<String> list = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                //skip the empty lines                
                if (line.trim().length() > 2) {
                    String[] fields = line.trim().split(";");
                    //Check if the lines have 3 fields           
                    if (fields.length == 3) {
                        Contact c = new Contact(fields[0], fields[1], fields[2]);
                        list.add(c);
                    }
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return list;
    }

    private boolean saveFile(String fileName, List<Contact> list) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            //foreach syntax
            for (Contact c : list) {
                bw.write(c.toString());
                bw.newLine();
            }
            return true;
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            return false;
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ContactsManager fw = new ContactsManager();
                fw.setVisible(true);
            }
        });
    }
}
