# Example for OO in Java
This repository contains examples for the laboratory classes of the lesson "Μεθοδολογίες Ανάπτυξης Εφαρμογών/Methodologies for Developing Applications" of the [Department of Informatics](http://www.cs.teiath.gr/en) of Technological Educational Institute of Athens.

The examples were developed using the NetBeans IDE and are structured in JAVA Packages. More details on how to use Git in NetBeans can be found [here](https://netbeans.org/kb/docs/ide/git.html). For the laboratory class purposes, you will need only the _clone_ feature of Git.

#Online Tutorials

* [Oracle Java Tutorials Learning Paths](http://docs.oracle.com/javase/tutorial/tutorialLearningPaths.html)
* [Tutorialspoint](http://www.tutorialspoint.com/java/)
* [LearnJavaOnline.org](http://www.learnjavaonline.org/)
* [NetBeans IDE Java Quick Start Tutorial](https://netbeans.org/kb/docs/java/quickstart.html)
